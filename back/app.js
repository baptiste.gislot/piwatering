const express = require('express');
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const indexRouter = require('./Routes/index');

dotenv.config({ path: (__dirname, './.env')});

const port = (process.env.PORT || '3000');
const dbUrl = process.env.MONGO_DB;

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/', indexRouter);


mongoose.connect(dbUrl, {
  useMongoClient: true
  })
  .then(() => {
    console.log('Connected to DB !');
  })
  .catch(() => {
    console.log('Something went wrong...');
  });

app.listen(port, () => {
  console.log(`Server is listening on port: ${port}`);
});