const express = require('express');
const WateringController = require("../Controllers/watering");
const MeasurementController = require("../Controllers/measurement");
const PreferenceController = require("../Controllers/preference");
const SensorController = require("../Controllers/sensor");

const router = express.Router();

router.get('/measurements/all/:sensorName', MeasurementController.getAllMeasurements);
router.get('/measurements/month/:sensorName', MeasurementController.getLastMonthMeasurements);
router.get('/measurements/week/:sensorName', MeasurementController.getLastWeekMeasurements);
router.get('/measurements/day/:sensorName', MeasurementController.getLastDayMeasurements);
router.get('/measurements/hour/:sensorName', MeasurementController.getLastHourMeasurements);
router.get('/measurements/minute/:sensorName', MeasurementController.getLastMinuteMeasurements);
router.post('/measurement/:sensorName', MeasurementController.setMeasurement);

router.get('/waterings/:sensorName', WateringController.getWatering);

router.get('/sensors', SensorController.getSensorNames);
router.get('/sensors/watering/:sensorName', SensorController.watering);

router.get('/preferences/:sensorName', PreferenceController.getPreference);
router.put('/preferences/:sensorName', PreferenceController.updatePreferences);


module.exports = router;