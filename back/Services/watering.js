const watering = require('../Models/watering');
const preferenceService = require('./preference');
const sensorService = require('./sensor');
const wateringService = require('./watering');

exports.getLastWatering = async (sensorName) => {
  return await watering.findOne({ sensorName }).sort({ timestamp: -1 });
};

exports.setWatering = async (capacity, sensorName) => {
  return watering.create({ capacity, sensorName});
};

exports.getWaterings = async (sensorName) => {
  return await watering.find({ sensorName });
};

exports.wateringIfNeeded = async (currentCapacity, sensorName) => {
  const preferences = await preferenceService.getPreference(sensorName);
  if (await isLastWateringTimeBufferPassed(preferences, sensorName) && currentCapacity > preferences.capacityBuffer) {
    wateringService.setWatering(currentCapacity, sensorName);
    sensorService.water(preferences.wateringTimeInSeconds, sensorName);
    return true
  } else {
    return false
  }
};

const isLastWateringTimeBufferPassed = async (preferences, sensorName) => {
  const lastMeasurement = await wateringService.getLastWatering(sensorName);
  const now = new Date().getTime();
  if (lastMeasurement) {
      let lastMeasurementTime = lastMeasurement.timestamp;
      lastMeasurementTimePlusBuffer = lastMeasurementTime.setMinutes(lastMeasurementTime.getMinutes() + preferences.minWateringIntervalInMinutes);
      return now > lastMeasurementTimePlusBuffer;
  } else return true;
};