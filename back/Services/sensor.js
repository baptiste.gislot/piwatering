const dotenv = require('dotenv');
const Gpio = require('onoff').Gpio;
const preferenceService = require("../Services/preference");

dotenv.config();

exports.water = async (wateringTimeInSeconds, sensorName) => {
    const preferences = await preferenceService.getPreference(sensorName);
    console.log("Starting to water...");
    const pump = new Gpio(preferences.signalPin, 'out');
    pump.writeSync(1);
    setTimeout(() => pump.writeSync(0), wateringTimeInSeconds * 1000);
    pump.unexport;
    return "Success";
}

exports.getSensorNames = async () => {
    const preferences = await preferenceService.getPreferences();
    return preferences.map(preference => preference.sensorName);
}