const measurementService = require("../Services/measurement");
const wateringService = require("../Services/watering");

exports.getLastMinuteMeasurements = async (req, res) => {
    const queryFilter = {
        sensorName: req.params.sensorName,
        timestamp: {
            $lte: new Date(),
            $gte: new Date(new Date().setMinutes(new Date().getMinutes() - 1))
        }
    };
    res.json(await measurementService.getSecondlyMeasurements(queryFilter));
};

exports.getLastHourMeasurements = async (req, res) => {
    const queryFilter = {
        sensorName: req.params.sensorName,
        timestamp: {
            $lte: new Date(),
            $gte: new Date(new Date().setHours(new Date().getHours() - 1))
        }
    };
    res.json(await measurementService.getMinutelyMeasurements(queryFilter));
};

exports.getLastDayMeasurements = async (req, res) => {
    const queryFilter = {
        sensorName: req.params.sensorName,
        timestamp: {
            $lte: new Date(),
            $gte: new Date(new Date().setDate(new Date().getDate() - 1))
        }
    };
    res.json(await measurementService.getHourlyMeasurements(queryFilter));
};

exports.getLastWeekMeasurements = async (req, res) => {
    const queryFilter = {
        sensorName: req.params.sensorName,
        timestamp: {
            $lte: new Date(),
            $gte: new Date(new Date().setDate(new Date().getDate() - 7))
        }
    };
    res.json(await measurementService.getHourlyMeasurements(queryFilter));
};

exports.getLastMonthMeasurements = async (req, res) => {
    const queryFilter = {
        sensorName: req.params.sensorName,
        timestamp: {
            $lte: new Date(),
            $gte: new Date(new Date().setMonth(new Date().getMonth() - 1))
        }
    };
    res.json(await measurementService.getDailyMeasurements(queryFilter));
};

exports.getAllMeasurements = async (req, res) => {
    const queryFilter = {
        sensorName: req.params.sensorName
    };
    res.json(await measurementService.getDailyMeasurements(queryFilter));
};

exports.setMeasurement = async (req, res) => {
    console.log(req.body.capacity);
    const result = await measurementService.setMeasurement(req.body.capacity, req.params.sensorName);
    const irrigated = await wateringService.wateringIfNeeded(req.body.capacity, req.params.sensorName);
    res.json({ ...result, irrigated });
};