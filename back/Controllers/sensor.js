const wateringService = require("../Services/watering");
const sensorService = require("../Services/sensor");
const preferenceService = require("../Services/preference");

exports.getSensorNames = async (req, res) => {
    res.json(await sensorService.getSensorNames());
};

exports.watering = async (req, res) => {
    const preferences = await preferenceService.getPreference(req.params.sensorName);
    const sensorResult = await sensorService.water(preferences.wateringTimeInSeconds, req.params.sensorName);
    if (sensorResult) {
        await wateringService.setWatering(null).then(() => {
            res.status(200);
            res.json("Success");
        });
    } else {
        res.status(500);
        res.json("Failed");
    }

};