const wateringService = require("../Services/watering");

exports.getWatering = async (req, res) => {
    res.json(await wateringService.getWaterings(req.params.sensorName));
};