const preferenceService = require("../Services/preference");

exports.getPreference = async (req, res) => {
    res.json(await preferenceService.getPreference(req.params.sensorName));
};

exports.updatePreferences = async (req, res) => {
    res.json(await preferenceService.updatePreferences(req.body, req.params.sensorName));
};