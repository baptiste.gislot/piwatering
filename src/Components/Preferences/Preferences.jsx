/** @jsx jsx */
import { jsx } from "theme-ui";
import "./Preferences.css";
import { useState, useEffect } from "react";

const Preferences = props => {
  const sensorName = props.sensorInFocus
  const capacityFactor = 100000
  const [preferences, setPreferences] = useState({
    minWateringIntervalInMinutes: 0,
    wateringTimeInSeconds: 0,
    capacityBuffer: 0,
    signalPin: 0
  });
  const [databasePreferences, setDatabasePreferences] = useState({
    minWateringIntervalInMinutes: 0,
    wateringTimeInSeconds: 0,
    capacityBuffer: 0,
    signalPin: 0
  });

  useEffect(() => {
    if (sensorName) {
      fetch(`${process.env.REACT_APP_BACKEND_URL}/preferences/${sensorName}`)
        .then(res => res.json())
        .then(
          async (preferences) => {
            console.log(preferences);
            setDatabasePreferences({ ...preferences, capacityBuffer: parseInt(capacityFactor / preferences.capacityBuffer) })
            setPreferences({ ...preferences, capacityBuffer: parseInt(capacityFactor / preferences.capacityBuffer) })
          },
          (error) => {
            console.log(`Coudn't fetch data. Error: ${error}`)
          }
        )
    }
  }, [setPreferences, sensorName])


  const updatePreferences = (e, key) => {
    fetch(`${process.env.REACT_APP_BACKEND_URL}/preferences/${sensorName}`, {
      headers: { 'Content-Type': 'application/json', },
      method: 'PUT',
      body: JSON.stringify({ ...preferences, capacityBuffer: parseInt(capacityFactor / preferences.capacityBuffer) })
    })
      .then(res => res.json())
      .then(
        async (preferences) => {
          setDatabasePreferences({ ...preferences, capacityBuffer: parseInt(capacityFactor / preferences.capacityBuffer) })
        },
        (error) => {
          console.log(`Coudn't fetch data. Error: ${error}`)
        }
      )
  }

  const preferenceBorderColor = (key) => {
    return preferences[key] === databasePreferences[key] ? "#161A30 !important" : "var(--primary) !important"
  }

  const preferenceButtonColor = () => {
    return JSON.stringify({
      minWateringIntervalInMinutes: preferences.minWateringIntervalInMinutes,
      wateringTimeInSeconds: preferences.wateringTimeInSeconds,
      capacityBuffer: preferences.capacityBuffer,
      signalPin: preferences.signalPin
    }) === JSON.stringify({
      minWateringIntervalInMinutes: databasePreferences.minWateringIntervalInMinutes,
      wateringTimeInSeconds: databasePreferences.wateringTimeInSeconds,
      capacityBuffer: databasePreferences.capacityBuffer,
      signalPin: databasePreferences.signalPin
    }) ? "#161A30 !important" : "var(--primary) !important"
  }

  return (
    <h3 sx={{ color: "text" }}>
      <div className="preference">
        <h3>Watering time [s]:</h3>
        <input sx={{ color: "text", borderColor: preferenceBorderColor("wateringTimeInSeconds") }} type="number"
          onChange={
            (e) => setPreferences({ ...preferences, wateringTimeInSeconds: parseInt(e.target.value) })
          }
          value={preferences.wateringTimeInSeconds} />
      </div>
      <div className="preference">
        <h3>Minimum watering time interval [min]:</h3>
        <input sx={{ color: "text", borderColor: preferenceBorderColor("minWateringIntervalInMinutes") }} type="number"
          onChange={(e) => setPreferences({ ...preferences, minWateringIntervalInMinutes: parseInt(e.target.value) })}
          value={preferences.minWateringIntervalInMinutes} />
      </div>
      <div className="preference">
        <h3>Minimum soil moisture:</h3>
        <input sx={{ color: "text", borderColor: preferenceBorderColor("capacityBuffer") }} type="number"
          onChange={(e) => setPreferences({ ...preferences, capacityBuffer: parseInt(e.target.value) })}
          value={preferences.capacityBuffer} />
      </div>
      <div className="preference">
        <h3>Signal Pin:</h3>
        <input sx={{ color: "text", borderColor: preferenceBorderColor("signalPin") }} type="number"
          onChange={(e) => setPreferences({ ...preferences, signalPin: parseInt(e.target.value) })}
          value={preferences.signalPin} />
      </div>
      <div className="preference">
        <div></div>
        <button sx={{ color: "text", borderColor: preferenceButtonColor() }} onClick={() => updatePreferences()}>Submit</button>
      </div>
    </h3>
  );
};

export default Preferences;