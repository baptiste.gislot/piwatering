/** @jsx jsx */
import { jsx } from "theme-ui";
import Header from './Components/Header/Header';
import Card from "./Components/Card/Card";
import IoTButton from "./Components/IoTButton/IoTButton";
import LineChart from "./Components/LineChart/LineChart";
import Preferences from "./Components/Preferences/Preferences";
import SensorPicker from "./Components/SensorPicker/SensorPicker";
import { useState, useEffect } from "react";
import './App.css';

const App = () => {
  const [sensors, setSensors] = useState([]);
  const [sensorInFocus, setSensorInFocus] = useState();

  useEffect(() => {
    getSensors();
  }, [setSensors]);

  const getSensors = () => {
    fetch(`${process.env.REACT_APP_BACKEND_URL}/sensors`)
      .then(res => res.json())
      .then(async (sensors) => {
        setSensors(sensors);
        setSensorInFocus(sensors[0]);
      }, (err) => {
        console.log(`Could not retrieve data. Error: ${err}`);
      });
  };

  return (
    <div className="App">
      <Header />
      <div className="main">
        <div className="container row">
          <div className="col-12">
            <SensorPicker sensors={sensors} setSensorInFocus={setSensorInFocus} sensorInFocus={sensorInFocus} />
          </div>
          <div className="col-md-12">
            <Card title='Statistics' body={<LineChart sensorInFocus={sensorInFocus} />} />
          </div>
          <div className="col-md-6">
            <Card title="Preferences" body={<Preferences sensorInFocus={sensorInFocus} />} />
          </div>
          <div className="col-md-6">
            <Card body={<IoTButton sensorInFocus={sensorInFocus} />} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
